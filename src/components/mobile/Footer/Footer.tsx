import Link from 'next/link';
import React from 'react';
import Image from 'next/image';

const SOCIAL_NETWORKS = [
  {
    src: '/social_media/discord.svg',
    url: 'https://discord.deepn.app/'
  },
  {
    src: '/social_media/facebook.svg',
    url: 'www.facebook.com',
  },
  {
    src: '/social_media/twitter.svg',
    url: 'https://twitter.com/DeepnIO',
  },
  {
    src: '/social_media/instagram.svg',
    url: 'https://www.instagram.com/deepn.app/',
  },
]

const Footer =  () => {
  return (
    <section className={'min-h-mobile-component-full flex flex-col pb-5 pt-5'} id={'Contact'}>
      <section className='flex flex-col space-y-3 items-center px-3.5'>
        <span className='text-4xl text-purple font-bold'>Coming Soon</span>
        <span className='text-little text-greyBlue text-center'>We are working hard on it</span>
      </section>
      <section className={'flex-1 flex flex-col justify-end items-center'}>

        <section className={'flex flex-col items-center space-y-4 w-full'}>
          <input type='email' className="bg-greyPurple h-12 rounded-full px-5 w-4/5 text-greyBlue outline-none" placeholder={'Email'}/>
          <div className={'border-3 border-purple font-bold bg-purple text-white rounded-full flex justify-center items-center py-2 px-1 text-sm w-32'}>
            <Link href={'https://dev.deepn.app/register'}>
              <a className={'w-full text-center'}>Notify me</a>
            </Link>
          </div>
        </section>
      </section>


      <section className={'flex-1 flex flex-col justify-end items-center w-full pt-16'}>
        <footer className='lg:h-16 flex flex-col justify-center z-50 w-full'>
          <section className='lg:h-16 flex justify-center'>
            <Image src={'/logo/logo.svg'} height={36} width={160} alt={'logo'} priority={true}/>
          </section>

          <div>
            <div className='bg-purple600 h-2px my-3 w-full my-5'/>
          </div>
          <section className='lg:h-16 flex flex-col items-center space-y-2.5'>
            <section className={'flex space-x-7 items-center'}>
              {SOCIAL_NETWORKS.map(({url,src}) => (
                <div role={'button'} key={url}>
                  <img src={src} style={{width: 20, height: 20}}/>
                </div>
              ))}
            </section>
            <span className='text-greyBlue text-xs font-bold'>© {new Date().getUTCFullYear()} Deepn, All rights reserved</span>
          </section>
        </footer>
      </section>
    </section>
  )
}

export default Footer;
