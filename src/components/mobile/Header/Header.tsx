import Image from 'next/image'
import Link from 'next/link'
import React, {
  useEffect,
  useState,
} from 'react';
import Typist from 'react-text-typist';
import clsx from 'clsx';

const Header = () => {
  const isIphone5 = window.innerWidth < 414
  return (
    <section className={'min-h-mobile-component-full w-full flex flex-col relative'} id={'Home'}>
      <div className={'pl-4'}>
        <span className={'font-bold text-greyBlue text-little'}>Let&apos;s make profit together !</span>
        <section className={'flex flex-col'}>
          <h1 className={clsx('font-bold', {'text-4.5xl': !isIphone5, 'text-4xl': isIphone5})}>Trading becomes</h1>
          <h2 className={clsx('font-bold', {'text-4.5xl': !isIphone5, 'text-4xl': isIphone5})}>
            <span className="relative z-0">
                <div className="w-max text-purple relative z-50 inline-block">
                  really
                </div>
                <div className="bg-purple600 opacity-20 absolute top-2.5 bottom-1.5 inset-0 rounded-10 fixedHeight -mx-2 rounded-2xl" />
            </span>
              <Typist className={'pl-4'} sentences={['smart', 'cool', 'effective', 'powerful', 'impressive', 'useful']}
                      startDelay={200}
                      typingSpeed={100}
                      deletingSpeed={100}
                      pauseTime={1000}
                      cursorClassName={'typingCursor'}
                      showCursor={false}
                      loop={true}/>
            </h2>
        </section>
        <div className='flex flex-col'>
          <span className={'text-greyBlue text-base leading-5 pt-5'}>Deepn will help you skyrocket your beloved portfolio thanks to the power of Artificial Intelligence</span>
        </div>
        <section className='flex space-x-5 pt-4'>
          <div className={'border-3 border-purple font-bold bg-purple text-white rounded-full flex justify-center items-center py-1 px-1 text-sm w-32'}>
            <Link href={'https://dev.deepn.app/register'}>
              <a className={'w-full text-center'}> Discover Now</a>
            </Link>
          </div>
          <div className={'border-3 border-purple600  font-bold bg-white text-greyBlue rounded-full flex justify-center items-center py-1 px-1 text-sm w-32'}>
            <Link href={'https://dev.deepn.app/register'}>
              <a >Free Trial</a>
            </Link>
          </div>
        </section>
      </div>
      <section className='h-full flex items-center pt-6'>
        <img src={'/header/presentation.webp'}
             alt={'logo'}/>
      </section>
    </section>
  )
};

export default Header;
