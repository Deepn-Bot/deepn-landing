import React from 'react';
import Image from 'next/image';

interface Props {
  title: string,
  subtitle: string,
  text: string
  image: string,
  imageRight: boolean
}

const TextWithImage = ({title, subtitle, text, image}: Props) => {

  return (
      <section className='flex flex-col items-center'>
        <section className='flex-1 flex justify-end h-full'>
          <section className='flex justify-items-center'>
            <Image src={image} height={284} width={346} alt={image} priority={true}/>
          </section>
        </section>
        <section className='flex-1  flex justify-start h-full'>
          <section className='flex flex-col space-y-2.5 px-5 pr-10 justify-center'>
            <span className='text-4xl text-purple font-bold max-w-lg'>{title}</span>
            <span className={'text-greyBlue text-lg font-bold '}>{subtitle}</span>
            <span className={'text-greyBlue text-base font-bold pt-5 max-w-lg'}>{text}</span>
          </section>
        </section>
      </section>
    )
};

export default TextWithImage;
