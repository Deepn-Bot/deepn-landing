import Pricing from './Pricing';
import React from 'react';
import Image from 'next/image';
import Link from 'next/link';

interface Props {
  title: string,
  mostPopular?: boolean,
  price: string,
  features: Array<string>
}

const PriceCard = ({title, price, mostPopular = false, features} : Props) => {

  return (
    <div className={'inline-block'}>
    <section style={{ width: 300, height: 470 }} className='rounded-2xl bg-white flex flex-col items-center relative'>
      <span className='text-2xl font-bold mt-5'>{title}</span>
      {mostPopular && <span className='text-little text-purple font-bold -mt-2.5'>MOST POPULAR</span>}
      {!mostPopular && <span className='text-base text-purple font-bold -mt-2.5'>&nbsp;</span>}
      <div className='flex pt-3'>
        <div className={'flex items-start'}>
          <span className='text-3xl font-bold'>$</span>
        </div>
        <div className={'flex items-center'}>
          <span className={mostPopular ? 'text-5xl text-purple font-bold': 'text-5xl text-purple600 font-bold'}>{price}</span>
        </div>
        <div className={'flex items-end'}>
          <span className='text-xl text-greyBlueLight font-bold'>/month</span>
        </div>
      </div>
      <div className='bg-purple600 h-2px w-4/5 my-3'/>
      <section className='flex flex-col content-start w-full space-y-2.5'>
        {features.map((feature, index) => (
          <div key={feature} className='flex items-center pl-5 space-x-2'>
              <Image src={index === 0 && title !== 'PRO' ? '/pricing/check.svg' : '/pricing/feature.svg'} alt={'feature'} width={24} height={24}/>
            <span className={'text-little'}>{feature}</span>
          </div>
        ))}
      </section>
      <div className='absolute bottom-4'>
        {mostPopular && <div className={'border-3 border-purple font-bold bg-purple text-white rounded-full flex justify-center items-center py-1 px-1 text-sm w-32'}>
          <Link href={'https://dev.deepn.app/register'}>
            <a className={'w-full text-center'}>Free Trial</a>
          </Link>
        </div>}
        {!mostPopular &&
        <div className={'border-3 border-purple600  font-bold bg-white text-greyBlue rounded-full flex justify-center items-center py-1 px-1 text-sm w-32'}>
            <Link href={'https://dev.deepn.app/register'}>
                <a>Free Trial</a>
            </Link>
        </div>
        }

      </div>
    </section>
    </div>
  )

}

export default PriceCard;
