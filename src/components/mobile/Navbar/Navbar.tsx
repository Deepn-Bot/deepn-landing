import React, {
  useEffect,
  useState,
} from 'react';
import Image from 'next/image'
import Link from 'next/link'
import { Drawer } from '@material-ui/core';

const links = ['Home', 'Features', 'Pricing', 'Contact']


const Navbar = () => {

  const scrolled = true

  const [menuOpen, setMenuOpen] = useState(false)

  const goToHome = () => {
    window.scroll(0,0)
    setMenuOpen(false)
  }

  return (
    <>
      <Drawer anchor={'right'} open={menuOpen} className={'w-screen'}>
        <div className={'w-screen h-screen bg-purple'}>
          <section className={'absolute right-4 top-4'} onClick={() => setMenuOpen(false)}>
            <Image src={'/icons/close.svg'} height={20} width={20} alt={'logo'} priority={true}/>
          </section>

          <section className={'flex flex-col items-center w-full h-full justify-center space-y-5'}>
              <span  className={'text-white text-lg'} onClick={() => goToHome()}>
                  Home
              </span>
            {['Features', 'Pricing', 'Contact'].map(menu => (
              <span key={menu} className={'text-white text-lg'} onClick={() => setMenuOpen(false)}>
                <Link href={'#' + menu}>
                  {menu}
                </Link>
              </span>
            ))}
            <span className={'text-white text-lg'} onClick={() => setMenuOpen(false)}>
                <Link href={'https://dev.deepn.app/login'}>
                  Login
                </Link>
            </span>
            <div className={'border-3 border-white font-bold bg-white text-purple rounded-full flex justify-center items-center py-1 px-1 text-lg w-32'}>
              <Link href={'https://dev.deepn.app/register'}>
                <a className={'w-full text-center'}>Sign Up</a>
              </Link>
            </div>

            <section className={'flex space-x-5 pt-5'}>
              <div className="h-full flex items-center">
                <Image src={'/navbar/language.svg'} height={20} width={20} alt={'language.svg'}/>
              </div>
              <div className="h-full flex items-center">
                <Image src={'/navbar/brightness.svg'} height={20} width={20} alt={'brightness'}/>
              </div>
            </section>
          </section>
        </div>
      </Drawer>


      <nav id='navbar' className={'transition-all duration-200 bg-purple h-12 fixed w-full z-50 px-5'}>
        <section className='flex justify-between items-center h-full'>
          <Image src={scrolled ? '/logo/logo_sticky.svg' : '/logo/logo.svg'} height={25} width={115} alt={'logo'} priority={true}/>
          <Image src={'/icons/menu.svg'} height={20} width={20} alt={'logo'} priority={true} onClick={() => setMenuOpen(true)}/>
        </section>
      </nav>

    </>
    );
};

export default Navbar;
