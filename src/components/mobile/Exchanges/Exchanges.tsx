import React from 'react';

const exchanges1 = ['binance', 'bitmex', 'coinbase']
const exchanges2 = ['kraken', 'bitforex']

import Image from 'next/image'
import clsx from 'clsx';



const Exchanges = () => {
  return (
    <footer className='bg-purple flex flex-col items-center pt-10 pb-10'>
      <span className={'text-white font-bold text-little mb-5'}>Trade though your favorite exchange</span>
      <section className='flex justify-around w-full mb-4'>
        {exchanges1.map(exchange => (
          <div key={exchange}>
            <Image  src={`/exchanges/${exchange}.svg`} height={20} width={100} alt={exchange} priority={true}/>
          </div>
        ))}
      </section>
      <section className='flex justify-center w-full space-x-5'>
        {exchanges2.map(exchange => (
          <div key={exchange}>
            <Image  src={`/exchanges/${exchange}.svg`} height={20} width={100} alt={exchange} priority={true}/>
          </div>
        ))}
      </section>
    </footer>
  );
}

export default Exchanges;
