import React from 'react';

const exchanges = ['binance', 'bitmex', 'coinbase', 'kraken', 'bitforex']
import Image from 'next/image'
import clsx from 'clsx';


interface Props {
  small: boolean
}

const Exchanges = ({small}: Props) => {
  return (
    <footer className='bg-purple space-y-10 flex flex-col items-center pt-5 pb-5'>
      <span className={'text-white font-bold md:text-xl lg:text-2xl xl:text-2.5xl'}>Trade though your favorite exchange</span>
      <section className='flex space-x-10 justify-center'>
        {exchanges.map(exchange => (
          <div key={exchange} className={'md:w-24 lg:w-32 xl:w-36'}>
            <Image  src={`/exchanges/${exchange}.svg`} height={small ? 35 *0.8 : 35} width={small ? 175 *0.8 : 175} alt={exchange} priority={true}/>
          </div>
        ))}
      </section>
    </footer>
  );
}

export default Exchanges;
