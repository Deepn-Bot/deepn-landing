import Header from './Header/Header';
import TextWithImage from './TextWithImage/TextWithImage';
import Strategies from './Strategies/Strategies';
import Pricing from './Pricing/Pricing';
import Footer from './Footer/Footer';
import ReactFullpage from '../../@fullpage/react-fullpage/dist/react-fullpage';

interface Props {
  enabled: boolean
}
const DesktopVersion = ({enabled}: Props) => (
  <>
  {enabled && <ReactFullpage
    navigation
    render={(a: any) => (
      <div>

        <div className='section' key={'header'}>
          <Header reactFC={a.fullpageApi}/>
        </div>

        <div className='section'  key={'feature'}>
          <section className='min-h-full flex flex-col items-center justify-center' id='Features'>
            <TextWithImage
              title={'Take back control of your money with our AI'}
              subtitle={'Grow your portfolio like the world’s most sophisticated investors'}
              text={'Let the power of Artificial Intelligence trade for you while you experience a stress-free life. We’re making the most advanced institutional portfolio management software available in the market to everyone.'}
              image={'/features/01.webp'}
              imageRight={true}
            />
            <TextWithImage
              title={'An automated trading solution for everyone'}
              subtitle={'Put your emotion out of the line'}
              text={'Invest in one click in all the cryptocurrencies available in your exchanges. Whether you use our bot, create your own or buy it from someone else, we will give you the best automated trading experience possible.'}
              image={'/features/02.webp'}
              imageRight={false}
            />
          </section>
          <section className='absolute bottom-10 right-10'>

          </section>
        </div>

        <div className='section' key={'strategies'}>
          <Strategies/>
        </div>

        <div className='section'  key={'feature2'}>
          <section className='min-h-full flex flex-col items-center justify-center'>
            <TextWithImage
              title={'Advanced data will work for you day and night'}
              subtitle={'We offer you metrics from every corner of the market'}
              text={'It’s hard to create a good strategy with poor metrics. That’s why we offer all the most advanced and exclusive metrics from data scientists. Choose from our ever-growing metrics list for creating the best-performing strategy.'}
              image={'/features/03.webp'}
              imageRight={true}
            />
            <TextWithImage
              title={'Become a popular investor'}
              subtitle={'And be a hero for the community'}
              text={'Earn money by sharing your financial knowledge with the community. Sell your strategy on the Deepn market so everyone can benefit from it. Earn, share and contribute with your years of backed experience of trading and become a Deepn Certified Investor.'}
              image={'/features/04.webp'}
              imageRight={false}
            />
          </section>
        </div>

        <div className='section' key={'pricing'}>
          <Pricing/>
        </div>

        <div className='section' key={'footer'}>
          <Footer/>
        </div>


      </div>
    )}
  >

  </ReactFullpage>}
  </>
)

export default DesktopVersion;
