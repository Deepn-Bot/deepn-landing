import Image from 'next/image'
import React, { useState } from 'react';

const STRATEGIES = [
  {
    title: 'Trust the AI',
    text: 'Imagine commanding a world top performing trader so that he trades for you. That’s the same concept we offer you with our powerful AI. You just have to ask him and he will put money into your pockets.',
    image: '01_ai_brain.webp'
  },
  {
    title: 'Do you like algorithms ?',
    text: 'We also do. Be a master of algorithmic trading by developing your own, with our easy to use financial oriented language. DeepScript is a Deepn language that lets you build the best algorithmic scripts.',
    image: '02_algo_tablet.webp'
  },
  {
    title: 'Trade like someone you trust',
    text: 'Good choice, you can be part of this social revolution. Our social platform lets you copy master investors who have been proving their experience over the years. You can trust them because we do.',
    image: '03_copy_bitcoin.webp'
  },
  {
    title: 'More on a long term frame ?',
    text: 'We have what you need. Copy the portfolio of investors we have chosen. Leverage their years of experience in just one click. If you feel like it, you can also choose from investors you trust yourself, the choice is yours.',
    image: '04_long_chart.webp'
  }
]

const Strategies = () => {

  const [open, setOpen] = useState<number>(0)

  const images = STRATEGIES.map(strategy => <Image key={strategy.title}
                                                   src={`/strategies/${strategy.image}`}
                                                   alt={'brain'}
                                                   width={540} height={445} priority={true}/>)

  return (
    <section className='min-h-full bg-purple py-16 flex flex-col items-center justify-center' >
      <span className='text-4.5xl text-white font-bold pb-5 pt-10'>Choose from our four profit strategy</span>
      <span className='text-gray-200 text-lg font-bold pb-10'>We’ve done the work so you can benefit from it with ease</span>
      <section className='flex h-full justify-center items-center w-4/5'>
        <div className='flex-1 flex justify-end'>
          <div>
            {images[open]}
          </div>
        </div>
        <section className='flex-1'>
          {STRATEGIES.map((strategy, index) => (
            <div key={strategy.title} onClick={() => setOpen(index)}>
              <div className={open === index ? 'flex flex-col relative' : 'flex flex-col relative h-12 transition-all duration-500'} >
                <div className={open === index ? 'absolute top-1 right-0 duration-500' : 'absolute right-0 top-1 transform rotate-90 duration-500'}>
                  <Image src={'/icons/chevron.svg'} width={20} height={12} alt={'open_close'} />
                </div>
                <span className='text-2.5xl text-white font-bold mb-5'>{strategy.title}</span>
                <span className={open === index ? 'transition-all duration-1000 text-lg text-white transform opacity-100' : 'text-lg text-white transform opacity-0'}>{strategy.text}</span>
              </div>
              {index !== STRATEGIES.length - 1 && <div className='bg-purple600 h-px my-2'/>}
            </div>
          ))}

        </section>
      </section>
    </section>
  )
}

export default Strategies;
