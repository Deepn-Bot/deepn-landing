import Image from 'next/image'
import Link from 'next/link'
import React, {
  useEffect,
  useState,
} from 'react';
import Exchanges from '../Exchanges/Exchanges';
import Navbar from '../Navbar/Navbar';
import clsx from 'clsx';
import Typist from 'react-text-typist';

interface Props {
  reactFC: any
}

const Header = ({reactFC}: Props) => {

  const [imageHeight, setImageHeight] = useState<number>(622)
  const [small, setSmall] = useState<boolean>(false)

  useEffect(() => {
    const update = () => {
      console.log(window.outerHeight, document?.getElementById('textContent')?.offsetHeight)
      setSmall(window.outerHeight <= 698 || window.outerWidth <= 1024)

      setImageHeight((document?.getElementById('textContent')?.offsetHeight || 622) * 0.7)
    }

    update()

    document.onreadystatechange = () => update();
    window.addEventListener('resize', () => update())
  }, [])

  return (
    <section className={'min-h-full w-full flex flex-col relative justify-between'} id={'Home'}>
      <Navbar reactFC={reactFC} />

      <section className='h-full w-full flex justify-center flex-1 items-stretch' id={'textContent'}>
        <div>
          <section className='h-full flex flex-col justify-center space-y-6'>
            <span className={'font-bold text-greyBlue md:text-little lg:text-base xl:text-xl'}>Let&apos;s make profit together !</span>
            <section className={'flex flex-col'}>
              <h1 className={'font-bold md:text-4xl lg:text-5xl xl:text-6xl'}>Trading becomes</h1>
              <h2 className={'font-bold md:text-4xl lg:text-5xl xl:text-6xl'}>
                <span className="relative z-0">
                    <div className="w-max text-purple relative z-50 inline-block">
                      really
                    </div>
                    <div className="bg-purple600 opacity-20 absolute top-4 bottom-3 inset-0 rounded-10 fixedHeight -mx-2 rounded-2xl" />
                </span>
                <Typist className={'pl-4'} sentences={['smart', 'cool', 'effective', 'powerful', 'impressive', 'useful']}
                        startDelay={200}
                        typingSpeed={200}
                        deletingSpeed={100}
                        pauseTime={1500}
                        cursorClassName={'typingCursor'}
                        showCursor={false}
                        loop={true}/>
                </h2>

            </section>
            <section className={'flex flex-col'}>
              <span className={'text-greyBlue md:text-little lg:text-base xl:text-xl'}>Deepn will help you skyrocket your beloved portfolio thanks to the</span>
              <span className={'text-greyBlue md:text-little lg:text-base xl:text-xl'}>power of Artificial Intelligence</span>
            </section>
            <section className='flex space-x-5'>
              <div className='border-3 border-purple font-bold bg-purple text-white
              rounded-full flex justify-center items-center
              md:text-little lg:text-base xl:text-xl md:w-32 lg:w-36 xl:w-40 md:py-1 lg:py-2 xl:py-3'>
                <Link href={'https://dev.deepn.app/register'}>
                  <a className={'w-full text-center'}> Discover Now</a>
                </Link>
              </div>
                <div className='border-3 border-purple600 font-bold bg-white text-greyBlue
              rounded-full flex justify-center items-center
              md:text-little lg:text-base xl:text-xl md:w-32 lg:w-36 xl:w-40 md:py-1 lg:py-2 xl:py-3'>
                <Link href={'https://dev.deepn.app/register'}>
                  <a >Free Trial</a>
                </Link>
              </div>
            </section>
          </section>
        </div>


        <div>
          <section className='h-full flex items-center'>
            <img src={'/header/presentation.webp'}
                 style={{width: '100%', height: imageHeight, maxWidth: '100%' }}
                 alt={'logo'}/>
          </section>
        </div>
      </section>


      <div role='button' className={'md:invisible lg:visible absolute flex flex-col items-center bottom-44 left-1/2 transform -translate-x-1/2'} onClick={() => reactFC.moveTo(2)}>
        <div className="mouse"/>
        <p className='scroll-text text-center'>Scroll</p>
      </div>
      <Exchanges small={small}/>
    </section>
  )
};

export default Header;
