import Image from 'next/image'
import React from 'react';
import PriceCard from './PriceCard';

const Pricing = () =>{
  return (
    <section className={'min-h-full bg-purple flex flex-col items-center justify-center'} id={'Pricing'}>
      <span className='text-4.5xl text-white font-bold pb-5'>Choose your plan</span>
      <span className='text-gray-200 text-lg font-bold'>Choose the pricing plan that suits your preferences</span>
      <section className='flex h-full justify-center items-center w-4/5 pt-10 space-x-10'>
        <PriceCard title={'PRO'} price={'TBA'} features={[
          '10 financial assets',
          '100 open positions',
          'access to the marketplace',
          'algorithmic strategy',
          '1 simulated trading bot',
          '1% fee on winning trades',
        ]}/>
        <PriceCard title={'FLEX'} price={'TBA'} mostPopular features={[
          'access to PRO features',
          '25 financial assets',
          '250 open positions',
          'access to decentralized exchanges',
          '2 simulated trading bots',
          '0.5% fee on winning trades',
        ]}/>
        <PriceCard title={'MASTER'} price={'TBA'} features={[
          'access to FLEX features',
          '75 financial assets',
          '500 open positions',
          'smart strategy',
          'access to futures contracts',
          'access to beta program',
          '3 simulated trading bots',
          '0.2% fee on winning trades',
        ]}/>
      </section>
    </section>
  )
}

export default Pricing;
