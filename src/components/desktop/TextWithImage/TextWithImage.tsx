import React from 'react';
import Image from 'next/image';

interface Props {
  title: string,
  subtitle: string,
  text: string
  image: string,
  imageRight: boolean
}

const TextWithImage = ({title, subtitle, text, image, imageRight = true}: Props) => {

  return imageRight ? (
    <section className='flex items-center w-4/5'>
      <section className='flex-1  flex justify-end h-full'>
        <section className='flex flex-col space-y-2.5 px-5 justify-center'>
          <span className='text-4.5xl text-purple font-bold max-w-lg'>{title}</span>
          <span className={'text-greyBlue text-lg font-bold '}>{subtitle}</span>
          <span className={'text-greyBlue text-base font-bold pt-5 max-w-lg'}>{text}</span>
        </section>
      </section>
      <section className='flex-1  flex justify-start h-full'>
        <section className='flex justify-items-start pl-10' >
          <Image src={image} height={367} width={497} alt={image} priority={true}/>
        </section>
      </section>
    </section>
  ) :
    (
      <section className='flex items-center w-4/5'>
        <section className='flex-1  flex justify-end h-full'>
          <section className='flex justify-items-center pr-20'>
            <Image src={image} height={367} width={497} alt={image} priority={true}/>
          </section>
        </section>
        <section className='flex-1  flex justify-start h-full'>
          <section className='flex flex-col space-y-2.5 px-5 pl-10 justify-center'>
            <span className='text-4.5xl text-purple font-bold max-w-lg'>{title}</span>
            <span className={'text-greyBlue text-lg font-bold '}>{subtitle}</span>
            <span className={'text-greyBlue text-base font-bold pt-5 max-w-lg'}>{text}</span>
          </section>
        </section>
      </section>
    )
};

export default TextWithImage;
