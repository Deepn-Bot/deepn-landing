import React, {
  useEffect,
  useState,
} from 'react';
import Image from 'next/image'
import Link from 'next/link'

const links = ['Home', 'Features', 'Pricing', 'Contact']

interface Props {
  reactFC?: any,
}

const INDEXES: {[key: number] : number} = {
  0: 1,
  1: 2,
  2: 5,
  3: 6,
}

const Navbar = ({ reactFC = null } : Props) => {


  return (
    <nav id='navbar' className={'transition-all duration-200 bg-purple lg:h-16 md:h-14 flex justify-around w-full z-50'}>
      <section className='h-full flex justify-around items-center lg:w-5/6 md:w-full'>
        <img src={'/logo/logo_sticky.svg'} className='md:h-1/2 w-auto' alt={'logo'}/>

        <section className={'flex lg:space-x-8 md:space-x-5 items-center'}>
          {links.map((link, index) => (
            <div key={link}>
            {reactFC && <span role={'button'}
                    onClick={() => reactFC.moveTo(INDEXES[index])}
                    className={'text-white font-medium md:text-xs lg:text-base'}>{link}</span>}
            {!reactFC && <a href={'#' + link} className={'text-white font-medium md:text-xs lg:text-base'}>{link}</a>}
            </div>
          ))}
          <span className={'text-white font-medium '}>|</span>
          <Link href={'https://dev.deepn.app/login'}>
            <a className={'text-white font-medium md:text-xs lg:text-base'}>Login</a>
          </Link>
          <div className='md:text-xs font-bold bg-white text-purple rounded-full flex justify-center items-center py-3 lg:text-base px-7'>
            <Link href={'https://dev.deepn.app/register'}>
              <a className={'w-full text-center'}>Sign Up</a>
            </Link>
          </div>
          <div className="h-full flex items-center">
            <Image src={'/navbar/language.svg'} height={20} width={20} alt={'language.svg'}/>
          </div>
          <div className="h-full flex items-center">
            <Image src={'/navbar/brightness.svg'} height={20} width={20} alt={'brightness'}/>
          </div>
        </section>
      </section>
    </nav>
    );
};

export default Navbar;
