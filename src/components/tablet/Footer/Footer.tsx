import Link from 'next/link';
import React from 'react';
import Image from 'next/image';

const SOCIAL_NETWORKS = [
  {
    src: '/social_media/discord.svg',
    url: 'https://discord.deepn.app/'
  },
  {
    src: '/social_media/facebook.svg',
    url: 'www.facebook.com',
  },
  {
    src: '/social_media/twitter.svg',
    url: 'https://twitter.com/deepn_finance',
  },
  {
    src: '/social_media/instagram.svg',
    url: 'https://www.instagram.com/deepn.finance/',
  },
]

const Footer =  () => {
  return (
    <section className={'h-screen flex flex-col'} id={'Contact'}>
      <section className={'flex-1 flex flex-col justify-end items-center space-y-20'}>
        <section className='flex flex-col space-y-3 items-center'>
          <span className='text-5xl text-purple font-bold'>Coming Soon</span>
          <span className='text-xl text-greyBlue'>We are working hard on it</span>
        </section>
        <section className={'space-x-6'}>
          <input type='email' className="bg-greyPurple h-12 rounded-full px-5 w-96 text-greyBlue outline-none" placeholder={'Email'}/>
          <Link href={'https://dev.deepn.app/register'}>
            <a className='font-bold bg-purple px-6 py-3.5 text-white text-xl rounded-full'>Notify me</a>
          </Link>
        </section>
      </section>


      <section className={'flex-1 flex flex-col justify-end items-center w-full'}>
        <footer className='lg:h-16 flex flex-col justify-center z-50 w-4/5 pb-20'>
          <section className='lg:h-16 flex justify-between'>
            <Image src={'/logo/logo.svg'} height={36} width={160} alt={'logo'} priority={true}/>

            <section className={'flex space-x-10 items-center'}>
              {['Home', 'Features', 'Pricing', 'Contact'].map(link => (
                <Link key={link} href={'https://dev.deepn.app/login'}>
                  <a className='font-medium text-greyBlue'>{link}</a>
                </Link>
              ))}
            </section>
          </section>

          <div>
            <div className='bg-purple600 h-2px my-3 w-full my-5'/>
          </div>
          <section className='lg:h-16 flex justify-between'>
            <span className='text-greyBlue text-sm font-bold'>© 2021 Deepn, All rights reserved</span>

            <section className={'flex space-x-7 items-center'}>
              {SOCIAL_NETWORKS.map(({url,src}) => (
                <div role={'button'} key={url}>
                    <Image src={src} height={24} width={24} alt={'logo'} priority={true}/>
                </div>
              ))}
            </section>
          </section>
        </footer>
      </section>
    </section>
  )
}

export default Footer;
