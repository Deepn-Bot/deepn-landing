import React, { useEffect } from 'react';
import PriceCard from './PriceCard';

const Pricing = () => {

  useEffect(() => {
    if (process.browser) {
      const width = (document.getElementById('price_content')?.offsetWidth) || 0;
      document.getElementById('price_container')?.scroll((width / 2) - 510, 0);
    }
  }, []);

  return (
    <section className={' py-16 bg-purple flex flex-col items-center justify-center'}
             id={'Pricing'}>
      <span className="text-4xl text-white font-bold pb-5">Choose your plan</span>
      <span className="text-gray-200 text-base font-bold text-center">Choose the pricing plan that suits your preferences</span>
      <div className="w-full overflow-x-auto pt-5"
           id={'price_container'}>
        <section id="price_content"
                 className="justify-center flex pb-8 flex-row pricing-section space-x-5 min-w-full"
                 style={{ width: 450 * 3 }}>
          <PriceCard title={'PRO'}
                     price={'TBA'}
                     features={[
                       '10 financial assets',
                       '100 open positions',
                       'access to the marketplace',
                       'algorithmic strategy',
                       '1 simulated trading bot',
                       '1% fee on winning trades',
                     ]} />
          <PriceCard id={'center_price'}
                     title={'FLEX'}
                     price={'TBA'}
                     mostPopular
                     features={[
                       'access to PRO features',
                       '25 financial assets',
                       '250 open positions',
                       'access to decentralized exchanges',
                       '2 simulated trading bots',
                       '0.5% fee on winning trades',
                     ]} />
          <PriceCard title={'MASTER'}
                     price={'TBA'}
                     features={[
                       'access to FLEX features',
                       '75 financial assets',
                       '500 open positions',
                       'smart strategy',
                       'access to futures contracts',
                       'access to beta program',
                       '3 simulated trading bots',
                       '0.2% fee on winning trades',
                     ]} />
        </section>
      </div>

    </section>
  );
};

export default Pricing;
