import React from 'react';
import Image from 'next/image';
import Link from 'next/link';

interface Props {
  id?: any,
  title: string,
  mostPopular?: boolean,
  price: string,
  features: Array<string>
}

const PriceCard = ({title, price, mostPopular = false, features, id = title} : Props) => {

  return (
    <section id={id} style={{ width: 400, height: 595 }} className='rounded-2xl bg-white flex flex-col items-center relative'>
      <span className='text-2.5xl font-bold mt-5'>{title}</span>
      {mostPopular && <span className='text-base text-purple font-bold -mt-2.5'>MOST POPULAR</span>}
      {!mostPopular && <span className='text-base text-purple font-bold -mt-2.5'>&nbsp;</span>}
      <div className='flex pt-3'>
        <div className={'flex items-start'}>
          <span className='text-4.5xl font-bold'>$</span>
        </div>
        <div className={'flex items-center'}>
          <span className={mostPopular ? 'text-7xl text-purple font-bold': 'text-7xl text-purple600 font-bold'}>{price}</span>
        </div>
        <div className={'flex items-end'}>
          <span className='text-xl text-greyBlueLight font-bold'>/month</span>
        </div>
      </div>
      <div className='bg-purple600 h-2px w-4/5 my-3'/>
      <section className='flex flex-col content-start w-full space-y-5'>
        {features.map((feature, index) => (
          <div key={feature} className='flex items-center pl-5 space-x-2'>
            <Image src={index === 0 && title !== 'PRO' ? '/pricing/check.svg' : '/pricing/feature.svg'} alt={'feature'} width={24} height={24}/>
            <span className={'text-base'}>{feature}</span>
          </div>
        ))}
      </section>
      <div className='absolute bottom-7'>
        <Link href={'https://dev.deepn.app/register'}>
          <a className={mostPopular ?'text-white font-bold bg-purple px-6 py-3 text-xl rounded-full' :
            'border-3 border-purple600 text-greyBlue font-bold bg-white px-6 py-3 text-xl rounded-full'}>Free Trial</a>
        </Link>
      </div>
    </section>
  )

}

export default PriceCard;
