import Navbar from '../desktop/Navbar/Navbar';
import Header from './Header/Header';
import TextWithImage from '../mobile/TextWithImage/TextWithImage';
import Exchanges from '../desktop/Exchanges/Exchanges';
import Strategies from './Strategies/Strategies';
import Pricing from './Pricing/Pricing';
import Footer from './Footer/Footer';

interface Props {
  enabled: boolean
}

const TabletVersion = ({enabled}: Props) => (
  <>
    {enabled &&
      <>
          <Navbar/>
          <Header/>
          <Exchanges small={false}/>
          <div className='section space-y-5 pb-10'>
              <TextWithImage
                  title={'Take back control of your money with our AI'}
                  subtitle={'Grow your portfolio like the world’s most sophisticated investors'}
                  text={'Let the power of Artificial Intelligence trade for you while you experience a stress-free life. We’re making the most advanced institutional portfolio management software available in the market to everyone.'}
                  image={'/features/01.webp'}
                  imageRight={true}
              />
              <TextWithImage
                  title={'An automated trading solution for everyone'}
                  subtitle={'Put your emotion out of the line'}
                  text={'Invest in one click in all the cryptocurrencies available in your exchanges. Whether you use our bot, create your own or buy it from someone else, we will give you the best automated trading experience possible.'}
                  image={'/features/02.webp'}
                  imageRight={false}
              />
          </div>
          <Strategies/>
          <div className='section space-y-5 pt-10 pb-10'>
              <TextWithImage
                  title={'Advanced data will work for you day and night'}
                  subtitle={'We offer you metrics from every corner of the market'}
                  text={'It’s hard to create a good strategy with poor metrics. That’s why we offer all the most advanced and exclusive metrics from data scientists. Choose from our ever-growing metrics list for creating the best-performing strategy.'}
                  image={'/features/03.webp'}
                  imageRight={true}
              />
              <TextWithImage
                  title={'Become a popular investor'}
                  subtitle={'And be a hero for the community'}
                  text={'Earn money by sharing your financial knowledge with the community. Sell your strategy on the Deepn market so everyone can benefit from it. Earn, share and contribute with your years of backed experience of trading and become a Deepn Certified Investor.'}
                  image={'/features/04.webp'}
                  imageRight={false}
              />
          </div>
          <Pricing/>
          <Footer/>
      </>
    }
  </>
)

export default TabletVersion;
