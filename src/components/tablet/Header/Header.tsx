import Image from 'next/image'
import Link from 'next/link'
import React, {
  useEffect,
  useState,
} from 'react';
import clsx from 'clsx';
import Typist from 'react-text-typist';

const Header = () => {


  return (
    <section className={'pt-16 pl-16 w-full flex flex-col relative justify-between'} id={'Home'}>

      <section className='h-full w-full flex justify-start flex-1 items-stretch' id={'textContent'}>
        <div>
          <section className='h-full flex flex-col justify-center space-y-6'>
            <span className={'font-bold text-greyBlue md:text-base lg:text-lg'}>Let&apos;s make profit together !</span>
            <section className={'flex flex-col'}>
              <h1 className={'font-bold md:text-5xl lg:text-6xl'}>Trading becomes</h1>
              <h2 className={'font-bold md:text-5xl lg:text-6xl'}>
                <span className="relative z-0">
                    <div className="w-max text-purple relative z-50 inline-block">
                      really
                    </div>
                    <div className="bg-purple600 opacity-20 absolute top-4 bottom-3 inset-0 rounded-10 fixedHeight -mx-2 rounded-2xl" />
                  </span>
                  <Typist className={'pl-4'} sentences={['smart', 'cool', 'effective', 'powerful', 'impressive', 'useful']}
                          startDelay={200}
                          typingSpeed={100}
                          deletingSpeed={100}
                          pauseTime={1000}
                          cursorClassName={'typingCursor'}
                          showCursor={false}
                          loop={true}/>
                </h2>
            </section>
            <section className={'flex flex-col'}>
              <span className={'text-greyBlue md:text-lg lg:text-xl'}>Deepn will help you skyrocket your beloved portfolio thanks to the</span>
              <span className={'text-greyBlue md:text-lg lg:text-xl'}>power of Artificial Intelligence</span>
            </section>
            <section className='flex space-x-5'>
              <div className={'py-3 text-xl w-40 border-3 border-purple font-bold bg-purple text-white rounded-full flex justify-center items-center'}>
                <Link href={'https://dev.deepn.app/register'}>
                  <a className={'w-full text-center'}> Discover Now</a>
                </Link>
              </div>
                <div className='py-3 text-xl w-40 border-3 border-purple600 font-bold bg-white text-greyBlue rounded-full flex justify-center items-center'>
                <Link href={'https://dev.deepn.app/register'}>
                  <a >Free Trial</a>
                </Link>
              </div>
            </section>
          </section>
        </div>

      </section>
      <section className='w-full flex justify-end -mt-8'>
        <img src={'/header/presentation.webp'}
             alt={'logo'}/>
      </section>

    </section>
  )
};

export default Header;
