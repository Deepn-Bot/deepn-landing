const { colors } = require('./styles/theme/beta/color');

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './src/components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      height: {
        'component-full': 'calc(100vh - 4rem)',
        'mobile-component-full': 'calc(100vh - 3rem)',
        '2px': '2px'
      },
      minHeight: {
        'component-full': 'calc(100vh - 4rem)',
        'mobile-component-full': 'calc(100vh - 3rem)',
      },
      borderWidth: {
        '3': '3px'
      },
      fontSize: {
        '2.5xl': ['1.75rem', {
          lineHeight: '40px',
        }],
        '4.5xl': ['2.5rem', {
          lineHeight: '48px',
        }],
        'little': ['0.875rem', {
          lineHeight: '24px',
        }]
      },
      colors: {
        ...colors,
      },
      textColor: {
        ...colors,
      },
      borderColor: {
        ...colors,
      },
      backgroundColor: {
        ...colors,
      },
    },
    fontFamily: {
      sans: ['Gilroy'],
    },
  },
  screens: {
    'sm': '640px',
    'md': '768px',
    'lg': '1024px',
    'xl': '1280px',
    '2xl': '1536px',
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
