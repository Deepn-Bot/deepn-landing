const colors = {
  pinkLight: '#E8D9FF',
  greyDark: '#3F4754',
  greyPurple: '#E5E3EA',
  greyBlue: '#6B7789',
  greyBlueLight: '#9FA6BB',
  red: '#E35850',
  redLight: '#FCEEED',
  purple: '#8C40FF',
  yellow: '#D3C649',
  green: '#47D19F',
  greyPurpleLight: '#F5F2F9',
  greyHyperLight: '#FCFAFF',
  menuItemGrey: '#787878',
  purple600: '#BA8CFF',
};

module.exports = { colors };
