import '../styles/globals.css'
import type { AppProps } from 'next/app'
import Head from 'next/head'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <>
    <Head>
      <meta name="viewport" content="width=device-width, user-scalable=no;maximum-scale=1"/>
      <title>Deepn</title>
    </Head>
      <Component {...pageProps} />
    </>
  )
}
export default MyApp
