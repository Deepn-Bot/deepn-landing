import type { NextPage } from 'next';
import Head from 'next/head';
import DesktopVersion from '../src/components/desktop';
import MobileVersion from '../src/components/mobile';
import TabletVersion from '../src/components/tablet';

import {
  isDesktop,
  isMobile,
  isTablet,
} from 'react-device-detect';


const Home: NextPage = () => {


  return (
    <div>
      <Head>
        <title>Deepn - The Smartest Crypto Trading Bot</title>
        <meta name="viewport"
              content="width=device-width, user-scalable=no;maximum-scale=1" />
        <meta name="keywords" content="
        deepn, crypto, trading, tool, crypto, cryptocurrency, bot, robot, automated,
        cryptocurrency, artificial intelligence, strategy, exchange,
        binance, coinbase, kraken, ftx,
        crypto trading bot, trading bot, cryptocurrency trading bot,automated trading" />
        <meta name="description" content="Deepn is the best crypto trading bot currently available, 24/7 trading automatically in the cloud. Easy to use, powerful and extremely safe. Trade your cryptocurrency now with Deepn, the automated crypto trading bot." />
        <link rel="canonical" href="https://www.deepn.app/"/>

        <meta property="og:url" content="https://www.deepn.app/" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:description" content="Deepn is the best crypto trading bot currently available, 24/7 trading automatically in the cloud. Easy to use, powerful and extremely safe. Trade your cryptocurrency now with Deepn, the automated crypto trading bot." />

        <link rel="icon"
              href="/favicon.ico" />

      </Head>


      <div>
        <DesktopVersion enabled={isDesktop} />
        <MobileVersion enabled={isMobile && !isTablet} />
        <TabletVersion enabled={isTablet} />
      </div>


    </div>
  );
};

export default Home;
